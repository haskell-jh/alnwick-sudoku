-- | TODO
module Alnwick.Game where

import Data.Set (Set, empty)

{-
What /is/ a Sudoku game? It is a 9x9 grid of cells,
making for eighty-one cells in-total. This could be
represented by a list or vector of cells. Perhaps, starting
with the representation of the cell itself will prove more useful?

-}

newtype Game = Game [Cell]

hl :: String
hl = replicate 37 '-' ++ "\n"

instance Show Game where
  show :: Game -> String
  show (Game []) = hl
  show (Game cs) = hl ++ showRow row ++ show (Game rest)
    where
      row, rest :: [Cell]
      (row, rest) = splitAt 9 cs

      showRow :: [Cell] -> String
      showRow [] = "|\n"
      showRow (r : rs) = "|" ++ show r ++ "" ++ showRow rs

data Row = Row1 | Row2 | Row3 | Row4 | Row5 | Row6 | Row7 | Row8 | Row9
  deriving (Show, Eq)

data Column = ColA | ColB | ColC | ColD | ColE | ColF | ColG | ColH | ColI
  deriving (Show, Eq)

data CellLoc = CellLoc Column Row

data Cell = Cell
  { solution :: Maybe Digit
  , drafts :: Set Digit
  , location :: CellLoc
  }

instance Show Cell where
  show :: Cell -> String
  show (Cell Nothing _ _) = "   "
  show (Cell (Just d) _ _) = " " ++ show d ++ " "

data Digit = One | Two | Three | Four | Five | Six | Seven | Eight | Nine
  deriving (Eq)

instance Show Digit where
  show :: Digit -> String
  show One = "1"
  show Two = "2"
  show Three = "3"
  show Four = "4"
  show Five = "5"
  show Six = "6"
  show Seven = "7"
  show Eight = "8"
  show Nine = "9"

greeting :: String
greeting = "Hi, I'm Alnwick!"

emptyGame :: Game
emptyGame = Game $ replicate 81 $ Cell (Just One) empty (CellLoc ColA Row1)
