module Main where

import Alnwick.Game (greeting)

main :: IO ()
main = putStrLn greeting
